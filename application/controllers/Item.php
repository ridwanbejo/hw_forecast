<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('logged_in'))
		{
			redirect('auth/login');
		}
	}

	public function index()
	{
		$items = $this->db->get('items')->result();
		$content = $this->load->view('item/index', array("items" => $items), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function add()
	{
		$content = $this->load->view('item/add', "", true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function create()
	{
		$data = array (
				"name" => $this->input->post("name"),
				"description" => $this->input->post("description"),
			);

		$this->db->insert("items", $data);

		redirect('/item');
	}

	public function edit($id=null)
	{
		$item = $this->db->get_where("items", array("id" => $id))->row();

		$content = $this->load->view('item/edit', array("item"=>$item), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function update()
	{
		$data = array (
				"name" => $this->input->post("name"),
				"description" => $this->input->post("description"),
			);

		$this->db->where("id", $this->input->post("id"));
		$this->db->update("items", $data);

		redirect('/item');
	}

	public function delete($id=null)
	{
		$this->db->where("id",$id);
		$this->db->delete("items");

		redirect('/item');
	}
}
