<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login()
	{
		if ($_POST){

			$this->db->select('username');
			$this->db->from("users");
			$this->db->where(
								array(
									"username"=>$this->input->post('username'), 
									"password"=>md5($this->input->post("password"))
								)
							);

			$user = $this->db->get()->row();
			$this->session->set_userdata(array(
											"logged_in" => TRUE,
											"username" => $user->username
										));

			redirect('/');
		}
		else {
			$this->load->view('auth/login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('auth/login');
	}
}
