<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dataset extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		if (!$this->session->userdata('logged_in'))
		{
			redirect('auth/login');
		}
	}

	public function index()
	{
		$this->db->select('*, datasets.id as "dataset_id"');
		$this->db->from('datasets');
		$this->db->join('items', 'items.id = datasets.item_id');
		$datasets = $this->db->get()->result();

		$content = $this->load->view('dataset/index', array("datasets" => $datasets), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function add()
	{
		$items = $this->db->get('items')->result();
		$content = $this->load->view('dataset/add', array("items" => $items), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function create()
	{
		$data = array (
				"item_id" => $this->input->post("item"),
				"periode" => $this->input->post("periode"),
				"sold_item" => $this->input->post("sold_item"),
			);

		$this->db->insert("datasets", $data);

		redirect('/dataset');
	}

	public function edit($id=null)
	{
		$dataset = $this->db->get_where("datasets", array("id" => $id))->row();
		$items = $this->db->get('items')->result();
		$content = $this->load->view('dataset/edit', array("dataset"=>$dataset, "items"=>$items), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function update()
	{
		$data = array (
				"item_id" => $this->input->post("item"),
				"periode" => $this->input->post("periode"),
				"sold_item" => $this->input->post("sold_item"),
			);

		$this->db->where("id", $this->input->post('dataset_id'));
		$this->db->update("datasets", $data);

		redirect('/dataset');
	}

	public function delete($id=null)
	{
		$this->db->where("id",$id);
		$this->db->delete("datasets");

		redirect('/dataset');
	}
}
