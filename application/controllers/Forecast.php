<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forecast extends CI_Controller {

	public function index()
	{
		$items = $this->db->get('items')->result();
		$content = $this->load->view('forecast/index', array("items"=>$items), true);
		$this->load->view('layout/main', array("content" => $content) );
	}

	public function do_forecast(){
		$alpha = $this->input->post('alpha');
		$beta = $this->input->post('beta');
		$gamma = $this->input->post('gamma');

		$this->db->select('*, datasets.id as "dataset_id"');
		$this->db->from('datasets');
		$this->db->where('item_id', $this->input->post('item'));
		$this->db->order_by('periode', 'asc');
		$datasets = $this->db->get()->result();

		// ambil historical data
		$historical_data = array ();

		for($i=0; $i < count($datasets); $i++){
			$historical_data[] = ["label"=>$datasets[$i]->periode, "val"=>$datasets[$i]->sold_item];
		}

		$season_factor = count($historical_data) / 4;

		// mendapatkan deretan index data dan x
		$x = array();
		$x_index = array();
		for ($i = 0; $i < count($historical_data) - 4; $i++){
			$x[] = $i + 1;
		}

		for ($i = 0; $i < count($historical_data) + 1; $i++){
			$x_index[] = $i + 1;
		}

		// proses deseasonalizing
		$initial_forecasting_factor = array();
		$temp_factor = 0;
		
		for ($i = 0; $i < count($historical_data) - 3; $i++){
			for ($j = 0; $j < 4; $j++){
				$index = $i+$j;
				$temp_factor += $historical_data[$index]["val"];
			}

			$initial_forecasting_factor[] = $temp_factor / 4;
			$temp_factor = 0;
		}

		// menghitung x^2, x*y, y
		$deseasonalize = array();
		$x_pow_two = [];
		$x_pow_y = [];
		for ($i = 0; $i < count($initial_forecasting_factor) - 1; $i++)
		{
			$temp_value = ($initial_forecasting_factor[$i] + $initial_forecasting_factor[$i+1]) / 2;
			$deseasonalize[] = $temp_value;
			$x_pow_two[] = pow(($i + 1), 2);
			$x_multiply_y[] = ($i + 1) * $temp_value;
		}

		$sum_x = array_sum($x);
		$sum_y = array_sum($deseasonalize);
		$sum_x_pow_two = array_sum($x_pow_two);
		$sum_x_multiply_y = array_sum($x_multiply_y);

		// menghitung initial level dan trend untuk membentuk y = trend*x + level
		$initial_level = (($sum_y * $sum_x_pow_two) - ($sum_x * $sum_x_multiply_y)) / ((12 * $sum_x_pow_two) - (pow($sum_x, 2)));
		$initial_trend = (12 * $sum_x_multiply_y - $sum_x * $sum_y) / (12 * $sum_x_pow_two - pow($sum_x, 2));

		// membentuk estimasi D
		$estimasi_d = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$estimasi_d[] = $initial_trend * ($i + 1) + $initial_level;
		}

		// membentuk preseason
		$pre_season = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$pre_season[] = $historical_data[$i]["val"] / $estimasi_d[$i];
		}

		// membentuk initial season
		$initial_season = array();
		for($i=0; $i < 4; $i++){
			
			$temp_season_val = 0;
			for ($j=0; $j < $season_factor; $j++){
				$temp_season_val += $pre_season[$i+($j*$season_factor)];
			}

			$initial_season[] = $temp_season_val / 4;
		}

		// membentuk 4 level dan 4 trend pertama
		$levels = array();
		$trends = array();
		$seasonal = array();

		$seasonal[] = $initial_season[0];
		$levels[] = $alpha * $historical_data[0]["val"] / $initial_season[0] + (1 - $alpha ) * ($initial_level + $initial_trend);
		$trends[] = $beta * ( $levels[0] - $initial_level) + (1 - $beta) * $initial_trend;
		for ($i = 1; $i < 4; $i++) {
			$levels[] = $alpha * $historical_data[$i]["val"] / $initial_season[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
			$seasonal[] = $initial_season[$i];
		}

		// membentuk sisa seasonal, trend, dan level
		for ($i = 4; $i < count($historical_data); $i++) {
			$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];
			$levels[] = $alpha * $historical_data[$i]["val"] / $seasonal[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
		}

		$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];
		
		// perhitungan forecast
		$forecast = array ();
		$forecast[] = ($initial_level + $initial_trend) * $seasonal[0];

		for ($i=1; $i < count($seasonal); $i++){
			$forecast[] = ($levels[$i-1] + $trends[$i-1]) * $seasonal[$i];
		}

		// perhitungan galat
		$errors = array();
		for ($i=0; $i < count($forecast) -1; $i++ ){
			$errors[] = $historical_data[$i]["val"] - $forecast[$i];
		}

		$forecasting_result = $forecast[count($forecast)-1];
		
		// menghitung error
		$bias = array_sum($errors);
		$mad = 0;
		for ($i=0; $i<count($errors);$i++){
			$mad += abs($errors[$i]);
		}

		$mad = $mad / count($errors);
		
		$mse = 0;
		for ($i=0; $i<count($errors);$i++){
			$mse += pow($errors[$i], 2);
		}

		$mse = $mse / count($errors);

		$tracking_signal = $bias / $mad;
		
		// mengambil data barang
		$item = $item = $this->db->get_where("items", array("id" => $this->input->post('item')))->row();

		// membuat data untk grafik morris.js
		$morris_data = array();
		for ($i=0; $i < count($historical_data); $i++){
			$morris_data[] = array(
								"y"=>$historical_data[$i]['label'],
								"actual"=>$historical_data[$i]['val'],
								"regresi"=>$estimasi_d[$i],
								"forecast"=>$forecast[$i],
							);
		}


		$final = date("Y-m-d", strtotime("+1 month", strtotime($historical_data[count($historical_data)-1]['label'])) );
		$morris_data[] = array(
								"y"=>$final,
								"actual"=>null,
								"regresi"=>null,
								"forecast"=>$forecast[$i],
							);

		$data = array (
					"historical_data" => $historical_data,
					"forecasting_result"=>$forecasting_result,
					"estimasi_d"=>$estimasi_d,
					"levels"=>$levels,
					"trends"=>$trends,
					"seasonal"=>$seasonal,
					"forecast"=>$forecast,
					"errors"=>$errors,
					"item" => $item,
					"alpha" => $alpha,
					"beta" => $beta,
					"gamma" => $gamma, 
					"morris_data"=>$morris_data,
					"bias"=>$bias,
					"mad"=>$mad,
					"mse"=>$mse,
					"tracking_signal"=>$tracking_signal,
				);

		$content = $this->load->view('forecast/result', $data, true);
		$js = $this->load->view('forecast/index_js', $data, true);
		$this->load->view('layout/main', array("content" => $content, "include_js"=>$js) );
	}
	
	public function do_forecast_debug() {
		$alpha = $this->input->post('alpha');
		$beta = $this->input->post('beta');
		$gamma = $this->input->post('gamma');

		echo $alpha." ".$beta." ".$gamma;

		$this->db->select('*, datasets.id as "dataset_id"');
		$this->db->from('datasets');
		$this->db->where('item_id', $this->input->post('item'));
		$this->db->order_by('periode', 'asc');
		$datasets = $this->db->get()->result();

		// ambil historical data
		$historical_data = array ();

		for($i=0; $i < count($datasets); $i++){
			$historical_data[] = ["label"=>$datasets[$i]->periode, "val"=>$datasets[$i]->sold_item];
		}

		echo "<pre>";
		print_r($historical_data);
		echo "</pre>";

		$season_factor = count($historical_data) / 4;

		// mendapatkan deretan index data dan x
		$x = array();
		$x_index = array();
		for ($i = 0; $i < count($historical_data) - 4; $i++){
			$x[] = $i + 1;
		}

		for ($i = 0; $i < count($historical_data) + 1; $i++){
			$x_index[] = $i + 1;
		}

		// proses deseasonalizing
		$initial_forecasting_factor = array();
		$temp_factor = 0;
		
		for ($i = 0; $i < count($historical_data) - 3; $i++){
			echo $historical_data[$i]["val"]. ": ";
			
			for ($j = 0; $j < 4; $j++){
				$index = $i+$j;
				echo $historical_data[$index]["val"]." ";
				$temp_factor += $historical_data[$index]["val"];
			}

			echo $temp_factor / 4;
			$initial_forecasting_factor[] = $temp_factor / 4;
			$temp_factor = 0;

			echo "<br />";
		}

		// menghitung x^2, x*y, y
		$deseasonalize = array();
		$x_pow_two = [];
		$x_pow_y = [];
		for ($i = 0; $i < count($initial_forecasting_factor) - 1; $i++)
		{
			$temp_value = ($initial_forecasting_factor[$i] + $initial_forecasting_factor[$i+1]) / 2;
			$deseasonalize[] = $temp_value;
			$x_pow_two[] = pow(($i + 1), 2);
			$x_multiply_y[] = ($i + 1) * $temp_value;
		}

		echo "<br/>";

		print_r($x);
		echo "<br/>";
		echo "<br/>";
		
		print_r($deseasonalize);
		echo "<br/>";
		echo "<br/>";

		print_r($x_pow_two);
		echo "<br/>";
		echo "<br/>";
		
		print_r($x_pow_y);

		$sum_x = array_sum($x);
		$sum_y = array_sum($deseasonalize);
		$sum_x_pow_two = array_sum($x_pow_two);
		$sum_x_multiply_y = array_sum($x_multiply_y);

		echo $sum_x." ".$sum_y." ".$sum_x_pow_two." ".$sum_x_multiply_y;

		// menghitung initial level dan trend untuk membentuk y = trend*x + level
		$initial_level = (($sum_y * $sum_x_pow_two) - ($sum_x * $sum_x_multiply_y)) / ((12 * $sum_x_pow_two) - (pow($sum_x, 2)));
		$initial_trend = (12 * $sum_x_multiply_y - $sum_x * $sum_y) / (12 * $sum_x_pow_two - pow($sum_x, 2));

		echo "<br/>".$initial_level;
		echo "<br/>".$initial_trend;

		// membentuk estimasi D
		$estimasi_d = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$estimasi_d[] = $initial_trend * ($i + 1) + $initial_level;
		}

		echo "<br/>";
		echo "<br/>";
		print_r($estimasi_d);

		// membentuk preseason
		$pre_season = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$pre_season[] = $historical_data[$i]["val"] / $estimasi_d[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($pre_season);

		// membentuk initial season
		$initial_season = array();
		for($i=0; $i < 4; $i++){
			
			$temp_season_val = 0;
			for ($j=0; $j < $season_factor; $j++){
				$temp_season_val += $pre_season[$i+($j*$season_factor)];
			}

			// $initial_season[] = ($pre_season[$i] + $pre_season[$i+4]  + $pre_season[$i+8]  + $pre_season[$i+12]) / 4;
			$initial_season[] = $temp_season_val / 4;
		}

		echo "<br/>";
		echo "<br/>";
		print_r($initial_season);

		// membentuk 4 level dan 4 trend pertama
		$levels = array();
		$trends = array();
		$seasonal = array();

		$seasonal[] = $initial_season[0];
		$levels[] = $alpha * $historical_data[0]["val"] / $initial_season[0] + (1 - $alpha ) * ($initial_level + $initial_trend);
		echo "<br/><br/>".$levels[0]."<br/>";

		$trends[] = $beta * ( $levels[0] - $initial_level) + (1 - $beta) * $initial_trend;
		echo "<br/>".$trends[0]."<br/><br/>";
		
		for ($i = 1; $i < 4; $i++) {
			$levels[] = $alpha * $historical_data[$i]["val"] / $initial_season[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
			$seasonal[] = $initial_season[$i];
		}

		
		
		echo "<br/>";
		echo "<br/>";
		print_r($levels);

		echo "<br/>";
		echo "<br/>";
		print_r($trends);
		echo "<br/>";
		
		// membentuk sisa seasonal, trend, dan level
		for ($i = 4; $i < count($historical_data); $i++) {
			$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];
			$levels[] = $alpha * $historical_data[$i]["val"] / $seasonal[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
		}

		$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];

		echo "<br/>";
		echo "<br/>";
		print_r($seasonal);

		echo "<br/>";
		echo "<br/>";
		print_r($levels);

		echo "<br/>";
		echo "<br/>";
		print_r($trends);

		// perhitungan forecast
		$forecast = array ();
		$forecast[] = ($initial_level + $initial_trend) * $seasonal[0];

		echo $forecast[0];
		for ($i=1; $i < count($seasonal); $i++){
			$forecast[] = ($levels[$i-1] + $trends[$i-1]) * $seasonal[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($forecast);

		// perhitungan galat
		$errors = array();
		for ($i=0; $i < count($forecast) -1; $i++ ){
			$errors[] = $historical_data[$i]["val"] - $forecast[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($errors);

		echo "<br/><br/><br/>";
		echo "hasil peramalan: ".$forecast[count($forecast)-1];
	}

	public function do_forecast_sample()
	{
		$alpha = 0.25;
		$beta = 0.20;
		$gamma = 0.15;

		// ambil historical data
		$historical_data = array (
									["label"=>"q12000",  "val"=> 98],
									["label"=>"q22000",  "val" => 106],
									["label"=>"q32000",  "val" => 109],
									["label"=>"q42000",  "val" => 133],
									["label"=>"q12001",  "val" => 107],
									["label"=>"q22001",  "val" => 116],
									["label"=>"q32001",  "val" => 121],
									["label"=>"q42001",  "val" => 146],
									["label"=>"q12002",  "val" => 127],
									["label"=>"q22002",  "val" => 130],
									["label"=>"q32002",  "val" => 136],
									["label"=>"q42002",  "val" => 159],
									["label"=>"q12003",  "val" => 139],
									["label"=>"q22003",  "val" => 143],
									["label"=>"q32003",  "val" => 153],
									["label"=>"q42003",  "val" => 177],
								);
		$season_factor = count($historical_data) / 4;

		// mendapatkan deretan index data dan x
		$x = array();
		$x_index = array();
		for ($i = 0; $i < count($historical_data) - 4; $i++){
			$x[] = $i + 1;
		}

		for ($i = 0; $i < count($historical_data) + 1; $i++){
			$x_index[] = $i + 1;
		}

		// proses deseasonalizing
		$initial_forecasting_factor = array();
		$temp_factor = 0;
		
		for ($i = 0; $i < count($historical_data) - 3; $i++){
			echo $historical_data[$i]["val"]. ": ";
			
			for ($j = 0; $j < 4; $j++){
				$index = $i+$j;
				echo $historical_data[$index]["val"]." ";
				$temp_factor += $historical_data[$index]["val"];
			}

			echo $temp_factor / 4;
			$initial_forecasting_factor[] = $temp_factor / 4;
			$temp_factor = 0;

			echo "<br />";
		}

		// menghitung x^2, x*y, y
		$deseasonalize = array();
		$x_pow_two = [];
		$x_pow_y = [];
		for ($i = 0; $i < count($initial_forecasting_factor) - 1; $i++)
		{
			$temp_value = ($initial_forecasting_factor[$i] + $initial_forecasting_factor[$i+1]) / 2;
			$deseasonalize[] = $temp_value;
			$x_pow_two[] = pow(($i + 1), 2);
			$x_multiply_y[] = ($i + 1) * $temp_value;
		}

		echo "<br/>";

		print_r($x);
		echo "<br/>";
		echo "<br/>";
		
		print_r($deseasonalize);
		echo "<br/>";
		echo "<br/>";

		print_r($x_pow_two);
		echo "<br/>";
		echo "<br/>";
		
		print_r($x_pow_y);

		$sum_x = array_sum($x);
		$sum_y = array_sum($deseasonalize);
		$sum_x_pow_two = array_sum($x_pow_two);
		$sum_x_multiply_y = array_sum($x_multiply_y);

		echo $sum_x." ".$sum_y." ".$sum_x_pow_two." ".$sum_x_multiply_y;

		// menghitung initial level dan trend untuk membentuk y = trend*x + level
		$initial_level = (($sum_y * $sum_x_pow_two) - ($sum_x * $sum_x_multiply_y)) / ((12 * $sum_x_pow_two) - (pow($sum_x, 2)));
		$initial_trend = (12 * $sum_x_multiply_y - $sum_x * $sum_y) / (12 * $sum_x_pow_two - pow($sum_x, 2));

		echo "<br/>".$initial_level;
		echo "<br/>".$initial_trend;

		// membentuk estimasi D
		$estimasi_d = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$estimasi_d[] = $initial_trend * ($i + 1) + $initial_level;
		}

		echo "<br/>";
		echo "<br/>";
		print_r($estimasi_d);

		// membentuk preseason
		$pre_season = array();
		for($i=0; $i < count($x_index) - 1; $i++){
			$pre_season[] = $historical_data[$i]["val"] / $estimasi_d[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($pre_season);

		// membentuk initial season
		$initial_season = array();
		for($i=0; $i < 4; $i++){
			
			$temp_season_val = 0;
			for ($j=0; $j < $season_factor; $j++){
				$temp_season_val += $pre_season[$i+($j*$season_factor)];
			}

			// $initial_season[] = ($pre_season[$i] + $pre_season[$i+4]  + $pre_season[$i+8]  + $pre_season[$i+12]) / 4;
			$initial_season[] = $temp_season_val / 4;
		}

		echo "<br/>";
		echo "<br/>";
		print_r($initial_season);

		// membentuk 4 level dan 4 trend pertama
		$levels = array();
		$trends = array();
		$seasonal = array();

		$seasonal[] = $initial_season[0];
		$levels[] = $alpha * $historical_data[0]["val"] / $initial_season[0] + (1 - $alpha ) * ($initial_level + $initial_trend);
		echo "<br/><br/>".$levels[0]."<br/>";

		$trends[] = $beta * ( $levels[0] - $initial_level) + (1 - $beta) * $initial_trend;
		echo "<br/>".$trends[0]."<br/><br/>";
		
		for ($i = 1; $i < 4; $i++) {
			$levels[] = $alpha * $historical_data[$i]["val"] / $initial_season[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
			$seasonal[] = $initial_season[$i];
		}

		
		
		echo "<br/>";
		echo "<br/>";
		print_r($levels);

		echo "<br/>";
		echo "<br/>";
		print_r($trends);
		echo "<br/>";
		// membentuk sisa seasonal, trend, dan level
		for ($i = 4; $i < count($historical_data); $i++) {
			$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];
			$levels[] = $alpha * $historical_data[$i]["val"] / $seasonal[$i] + (1 - $alpha ) * ($levels[$i-1] + $trends[$i-1]);
			$trends[] = $beta * ( $levels[$i] - $levels[$i-1]) + (1 - $beta) * $trends[$i-1];
		}

		$seasonal[] = $gamma * $historical_data[$i-4]["val"] / $levels[$i - 4] + (1 - $gamma) * $seasonal[$i - 4];

		echo "<br/>";
		echo "<br/>";
		print_r($seasonal);

		echo "<br/>";
		echo "<br/>";
		print_r($levels);

		echo "<br/>";
		echo "<br/>";
		print_r($trends);

		// perhitungan forecast
		$forecast = array ();
		$forecast[] = ($initial_level + $initial_trend) * $seasonal[0];

		echo $forecast[0];
		for ($i=1; $i < count($seasonal); $i++){
			$forecast[] = ($levels[$i-1] + $trends[$i-1]) * $seasonal[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($forecast);

		// perhitungan galat
		$errors = array();
		for ($i=0; $i < count($forecast) -1; $i++ ){
			$errors[] = $historical_data[$i]["val"] - $forecast[$i];
		}

		echo "<br/>";
		echo "<br/>";
		print_r($errors);

		echo "<br/><br/><br/>";
		echo "hasil peramalan: ".$forecast[count($forecast)-1];
	}

}
