<!DOCTYPE html>
<html lang="en">

<?php $this->load->view("layout/header"); ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        
       		<?php $this->load->view('layout/nav'); ?>

       		<?php $this->load->view('layout/sidebar'); ?>

            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <?php echo $content; ?>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php $this->load->view('layout/footer'); ?>
    <?php if (isset($include_js)) echo $include_js; ?>
</body>

</html>
