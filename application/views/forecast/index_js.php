<script type="text/javascript">
	var morris_data = JSON.parse($('#morris-data').html());
	Morris.Line({
	  element: 'line-example',
	  data: morris_data,
	  xkey: 'y',
	  ykeys: ['actual', 'regresi', 'forecast'],
	  labels: ['Penjualan Aktual', 'Hasil Regresi', 'Peramalan Penjualan']
	});
</script>