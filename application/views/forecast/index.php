        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Inisialisasi Peramalan Holt Winter</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-7">
                <form role="form" method="POST" action="<?php echo site_url("forecast/do_forecast"); ?>">
                    
                    <div class="form-group">
                        <label>Barang</label>
                        
                        <select class="form-control" name="item">
                            <?php foreach ($items as $row): ?>
                            <option value="<?php echo $row->id ?>"><?php echo $row->name ?></option>
                            <?php endforeach; ?>
                        </select>

                        <p class="help-block">Pilih salah satu barang</p>
                    </div>

                    <div class="form-group">
                        <label>Parameter Alpha</label>
                        <input type="text" class="form-control" name="alpha" value="0.25">
                        <p class="help-block">Isi dengan angka koma, contoh: 0.25</p>
                    </div>

                    <div class="form-group">
                        <label>Parameter Beta</label>
                        <input type="text" class="form-control" name="beta"  value="0.2">
                        <p class="help-block">Isi dengan angka koma, contoh: 0.2</p>
                    </div>

                    <div class="form-group">
                        <label>Parameter Gamma</label>
                        <input type="text" class="form-control" name="gamma"  value="0.15">
                        <p class="help-block">Isi dengan angka koma, contoh: 0.15</p>
                    </div>

                    <button type="submit" class="btn btn-default btn-primary">Proses Peramalan</button>
                    <button type="reset" class="btn btn-default btn-warning">Reset</button>
                
                </form>
            </div>
        </div>