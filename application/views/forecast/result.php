			<div class="row">
	            <div class="col-lg-12">
	                <h1 class="page-header">Hasil Peramalan Holt Winter Terhadap Barang: <?php echo strtoupper($item->name); ?></h1>
	            </div>
	            <!-- /.col-lg-12 -->
	        </div>
	        <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Parameter</h2>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Alpha</th>
                                    <th>Beta</th>
                                    <th>Gamma</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                	<td><?php echo $alpha; ?></td>
                                    <td><?php echo $beta; ?></td>
                                    <td><?php echo $gamma; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="morris-data" style="display:none;"><?php echo json_encode($morris_data); ?></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Grafik Hasil Peramalan</h2>
                </div>
                <div class="col-lg-12">
                    <div id="line-example"></div>
                </div>
            </div>

			<div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Proses Peramalan</h2>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Penjualan Aktual</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($historical_data as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row['val']; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Nilai Regresi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($estimasi_d as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Level</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($levels as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Trend</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($trends as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Seasonal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($seasonal as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Forecast</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($forecast as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Error Peramalan</h2>
                </div>
                <div class="col-lg-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                	<th>#</th>
                                    <th>Error</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($errors as $row): ?>
                                <tr>
                                	<td><?php echo $counter; ?></td>
                                    <td><?php echo $row; ?></td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <tbody>
                                <tr>
                                	<td><b>Bias</b></td>
                                    <td>:</td>
                                    <td><?php echo $bias; ?></td>
                                </tr>
                                <tr>
                                	<td><b>Mean Absolute Deviation</b></td>
                                    <td>:</td>
                                    <td><?php echo $mad; ?></td>
                                </tr>
                                <tr>
                                	<td><b>Mean Squared Error</b></td>
                                    <td>:</td>
                                    <td><?php echo $mse; ?></td>
                                </tr>
                                <tr>
                                	<td><b>Tracking Signal</b></td>
                                    <td>:</td>
                                    <td><?php echo $tracking_signal; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
