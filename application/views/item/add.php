        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tambah Barang</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-8">
                <form role="form" method="POST" action="<?php echo site_url("item/create"); ?>">
                
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="name">
                        <p class="help-block">Isi nama dengan karakter alfabet</p>
                    </div>

                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea class="form-control" rows="5" name="description"></textarea>
                    </div>
                    
                    <button type="submit" class="btn btn-default btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-default btn-warning">Reset</button>
                
                </form>
            </div>
        </div>