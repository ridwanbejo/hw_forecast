            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Barang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($items as $row): ?>
                                <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->description; ?></td>
                                    <td style="text-align:center;">
                                        <a href="<?php echo site_url("item/edit/".$row->id) ?>" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="<?php echo site_url("item/delete/".$row->id) ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           