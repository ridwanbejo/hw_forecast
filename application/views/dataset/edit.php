        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Ubah Data Penjualan</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-7">
                <form role="form" method="POST" action="<?php echo site_url("dataset/update"); ?>">
                    
                    <div class="form-group">
                        <label>Periode</label>
                        
                        <select class="form-control" name="item">
                            <?php foreach ($items as $row): ?>
                            <?php if ($dataset->item_id == $row->id) { ?>
                            <option selected value="<?php echo $row->id ?>"><?php echo $row->name ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $row->id ?>"><?php echo $row->name ?></option>
                            <?php } ?>
                            <?php endforeach; ?>
                        </select>

                        <p class="help-block">Pilih salah satu barang</p>
                    </div>

                    <div class="form-group">
                        <label>Periode</label>
                        <input type="text" class="form-control" name="periode" value="<?php echo $dataset->periode ?>">
                        <input type="hidden" class="form-control" name="dataset_id"  value="<?php echo $dataset->id ?>">
                        <p class="help-block">Isi nama dengan format yyyy-mm-dd, contoh: 2016-05-20</p>
                    </div>

                    <div class="form-group">
                        <label>Jumlah Penjualan</label>
                        <input type="text" class="form-control" name="sold_item"  value="<?php echo $dataset->sold_item ?>">
                        <p class="help-block">Isi dengan angka</p>
                    </div>
                    
                    <button type="submit" class="btn btn-default btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-default btn-warning">Reset</button>
                
                </form>
            </div>
        </div>