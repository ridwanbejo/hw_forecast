            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Penjualan Barang</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Barang</th>
                                    <th>Periode</th>
                                    <th>Total Penjualan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $counter=1; foreach ($datasets as $row): ?>
                                <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->periode; ?></td>
                                    <td><?php echo $row->sold_item; ?></td>
                                    <td style="text-align:center;">
                                        <a href="<?php echo site_url("dataset/edit/".$row->dataset_id) ?>" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="<?php echo site_url("dataset/delete/".$row->dataset_id) ?>" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                    </td>
                                </tr>
                                <?php $counter++; endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
           