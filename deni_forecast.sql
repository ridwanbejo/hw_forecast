-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2016 at 05:31 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `deni_forecast`
--

-- --------------------------------------------------------

--
-- Table structure for table `datasets`
--

CREATE TABLE IF NOT EXISTS `datasets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periode` date NOT NULL,
  `sold_item` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `datasets_periode_idx` (`periode`) USING BTREE,
  KEY `datasets_items_fk` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `datasets`
--

INSERT INTO `datasets` (`id`, `periode`, `sold_item`, `item_id`) VALUES
(5, '2015-07-30', 1507, 1),
(7, '2015-08-30', 985, 1),
(8, '2015-09-30', 830, 1),
(9, '2015-10-30', 1250, 1),
(10, '2015-11-30', 1320, 1),
(11, '2015-12-30', 1101, 1),
(12, '2016-01-30', 804, 1),
(13, '2016-02-28', 571, 1),
(14, '2016-03-30', 551, 1),
(15, '2016-04-30', 587, 1),
(16, '2016-05-30', 1460, 1),
(17, '2016-06-30', 2630, 1),
(18, '2015-07-30', 305, 2),
(19, '2015-08-30', 256, 2),
(20, '2015-09-30', 264, 2),
(21, '2015-10-30', 307, 2),
(22, '2015-11-30', 50, 2),
(23, '2015-12-30', 101, 2),
(24, '2016-01-30', 234, 2),
(25, '2016-02-28', 168, 2),
(26, '2016-03-30', 267, 2),
(27, '2016-04-30', 117, 2),
(28, '2016-05-30', 540, 2),
(29, '2016-06-30', 1850, 2),
(30, '2015-07-30', 108, 8),
(31, '2015-08-30', 115, 8),
(33, '2015-09-30', 155, 8),
(34, '2015-10-30', 210, 8),
(35, '2015-11-30', 205, 8),
(36, '2015-12-30', 50, 8),
(37, '2016-01-30', 22, 8),
(38, '2016-02-28', 221, 8),
(39, '2016-03-30', 124, 8),
(40, '2016-04-30', 221, 8),
(41, '2016-05-30', 355, 8),
(42, '2016-06-30', 330, 8);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `items_name_idx` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`) VALUES
(1, 't-shirt', 'lorem ipsum sit dolor amet'),
(2, 'polo t-shirt', 'lorem ipsum sit dolor amet'),
(8, 'kemeja', 'lorem ipsum sit dolor amet kemeja');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'deni', '43f41d127a81c54d4c8f5f93daeb7118');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `datasets`
--
ALTER TABLE `datasets`
  ADD CONSTRAINT `datasets_items_fk` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
