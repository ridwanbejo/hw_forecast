#Cara Instalasi HW Forecast

- Download repositori ini dari bitbucket, pilih sebagai zip
- Ekstrak folder di htdocs XAMPP atau Apache yang Anda pasang di sistem operasi Anda
- Buat database dengan nama "deni_forecast" atau nama lain dengan menggunakan PHPMyAdmin
- Masuk ke database lalu pilih menu import, kemudian pilih file "deni_forecast.sql" yang ada di dalam folder hw_forecast untuk diimport yang telah mengandung tabel dan data contoh
- akses url http://localhost/hw_forecast/
- setelah muncul halaman login silahkan masukkan akun dengan username "admin" dan password "admin"
- Voilla! Anda sudah dapat menggunakan HW Forecast mulai dari mengelola data barang, data penjualan, dan mencoba forecast data penjualan berikutnya dengan menggunakan metode holt winter